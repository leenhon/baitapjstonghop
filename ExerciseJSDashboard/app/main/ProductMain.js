import { layThongTinTuForm, renderDanhSachSanPham, setLoadingOff, setLoadingOn, showThongTin } from "../controllers/ProductControllers.js";
import { Product } from "../model/ProductModel.js";


setLoadingOff();

// Function: Render danh sách sản phẩm
let renderDanhSachSanPhamServices = async () => {
    setLoadingOn();
    try {
        let response = await axios({
            url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
            method: "GET",
        });
        setLoadingOff();
        // console.log(response);
        console.log("Danh sách sản phẩm API:", response.data);
        let dsSanPham = response.data.map((sanPham) => {
            return new Product(sanPham.id, sanPham.name, sanPham.type, sanPham.price, sanPham.screen, sanPham.frontCamera, sanPham.backCamera, sanPham.quantity, sanPham.img, sanPham.desc);
        });
        console.log("Danh sách sản phẩm:", dsSanPham);
        renderDanhSachSanPham(dsSanPham);
    } catch (err) {
        console.log(err);
        setLoadingOff();
    }
}




// Function: Thêm Sản Phẩm
document.getElementById("btnThemSP").addEventListener("click", function () {
    let data = layThongTinTuForm();
    let sanPham = new Product(null, data.tenSP, data.hangSX, data.giaSP, data.screenSP, data.camFront, data.camBack, data.soLuong, data.hinhSP, data.moTa);
    console.log(sanPham);
    setLoadingOn();

    axios({
        url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
        method: "POST",
        data: sanPham,

    })
        .then((res) => {
            console.log("res", res);
            setLoadingOff();
            renderDanhSachSanPhamServices();
            $("#exampleModal").modal("hide");

            // alert("Thêm sản phẩm thành công!");
        }).catch((err) => {
            setLoadingOff();
            console.log("err", err);

        });
});


// Function: Xóa sản phẩm
let xoaSanPham = (id) => {
    setLoadingOn();
    axios({
        url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
        method: "DELETE",
    })
        .then((res) => {
            setLoadingOff();
            renderDanhSachSanPhamServices();

        })
        .catch((err) => {
            setLoadingOff();
            console.log("err", err);
        });
}



//Function: Sửa sản phẩm
let suaSanPham = (id) => {
    showThongTinSPModal(id);
    document.getElementById("btnCapNhat").addEventListener("click", function () {
        let data = layThongTinTuForm();
        let sanPham = new Product(data.id, data.tenSP, data.hangSX, data.giaSP, data.screenSP, data.camFront, data.camBack, data.soLuong, data.hinhSP, data.moTa);
        console.log("sản phẩm sửa: ",sanPham);
        setLoadingOn();

        axios({
            url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
            method: "PUT",
            data: sanPham,

        })
            .then((res) => {
                console.log("res api:", res);
                setLoadingOff();
                renderDanhSachSanPhamServices();
                $("#exampleModal").modal("hide");

                document.getElementById("btnThemSP").style.display = "inline-block";
                document.getElementById("btnCapNhat").style.display = "none";
                $('#productForm').find("input[type=text], textarea").val("");
                // alert("Cập nhật sản phẩm thành công!");
            }).catch((err) => {
                setLoadingOff();
                console.log("err", err);

            });
    });

}

// Function: lấy thông tin sản phẩm show lên form
let showThongTinSPModal = (id) => {
    $("#exampleModal").modal("show");

    // Lấy thông tin sản phẩm theo id
    axios({
        url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
        method: "GET",

    })
        .then((res) => {
            console.log("res data lên form:", res);
            showThongTin(res.data);
            // ẩn nút Thêm, hiện nút update
            document.getElementById("btnThemSP").style.display = "none";
            document.getElementById("btnCapNhat").style.display = "inline-block";
        }).catch((err) => {
            console.log("err", err);

        });


}



// Function: Reset form
document.getElementById("btnClose").addEventListener("click", function () {
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        $('#productForm').find("input[type=text], textarea").val("");
        document.getElementById("btnThemSP").style.display = "inline-block";
        document.getElementById("btnCapNhat").style.display = "none";
    })
})

// gán lại hàm
window.suaSanPham = suaSanPham;
window.xoaSanPham = xoaSanPham;

//================ LOAD DEFAULT ===============
renderDanhSachSanPhamServices();




