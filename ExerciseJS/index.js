import { Product } from "./product.js";


const Base_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";
let idSanPhamThemGioHang = null;


let gioHang = [];




// FUNCTION: HIỂN THỊ DANH SÁCH SẢN PHẨM 
let hienThiDanhSach = () => {
    axios({
        url: Base_URL,
        method: "GET",
    })
        .then(function (res) {
            console.log("Data API", res.data);
            renderTableSanPham(res.data);
        })
        .catch(function (err) {
            console.log("no", err);
        });
}


// FUNCTION: RENDER DANH SÁCH SẢN PHẨM
let renderTableSanPham = (data) => {
    let contentItem = data.map((item, index) => {
                     return `<div class="product-item" key=${index}>
                                <div class="product-content">
                                    <div class="product-image">
                                        <img src="${item.img}" alt="" />
                                    </div>
                                    <div class="product-info">
                                        <h4 class="product-name">${item.name}</h4>
                                        <h4 class="product-price">${item.price}</h4>
                                    </div>
                                    <div class="btn">
                                        <button type="button" onclick=themSanPhamVaoGioHang(${item.id})>Add to Cart</button>
                                    </div>
                                </div>
                            </div>`

    });
    return document.getElementById("product-list").innerHTML = contentItem;
}


// Sự kiện thay đổi select
document.getElementById("brand").addEventListener("change", function () {
    let resultChange = document.getElementById("brand").value;
    if (resultChange === "all") {
        hienThiDanhSach();
    } else {
        locSanPhamTheoHang(resultChange);
    }

});

// FUNCTION: LỌC SẢN PHẨM THEO HÃNG
let locSanPhamTheoHang = (brandName) => {
    let listProductByBrand = [];

    axios({
        url: Base_URL,
        method: "GET",
    })
        .then(function (res) {
            let listProducts = res.data;
            console.log(listProducts);
            // lọc mảng sản phẩm
            listProducts.forEach(item => {
                if (item.type.toLowerCase() === brandName.toLowerCase()) {
                            listProductByBrand.push(item);
                }
            });
            renderTableSanPham(listProductByBrand);
        })
        .catch(function (err) {
            console.log("no", err);
        });
}

// FUNCTION: THÊM SẢN PHẨM VÀO GIỎ HÀNG
let themSanPhamVaoGioHang = (id) => {
    let sanPhamThem = new Product();
    let flag = false;
    axios({
        url: `${Base_URL}/${id}`,
        method: "GET",
    })

        .then((res) => {
            for (let i = 0; i < gioHang.length; i++) {
                let sanPhamTrongGio = gioHang[i];
                // Kiểm tra đã có trong giỏ hàng
                if (+sanPhamTrongGio.sanpham.id === id) {
                    gioHang[i].soluong += 1;
                    flag = true;
                }
            }
            if (!flag) {
                sanPhamThem.sanpham = res.data;
                sanPhamThem.soluong = 1;
                gioHang.push(sanPhamThem);
            }

            renderCart(gioHang);
            tongTienThanhToan();
            saveCartInLocal();
        })
        .catch((err) => {
            console.log("no", err);
        });
}

// FUNCTION: RENDER GIỎ HÀNG
let renderCart = (data) =>{
    let trContent = data.map((item, index) => {
            return        ` <tr key=${index}>
                                <td class="td-img">
                                    <img src="${item.sanpham.img}" alt"" />  
                                </td>
                                <td> ${item.sanpham.name} </td>
                                <td> ${item.sanpham.price} </td>
                                <td> 
                                    <span>${item.soluong} </span>
                                    <button onclick=themSoLuong(${item.sanpham.id})><i class="fa-solid fa-angle-up"></i></button>
                                    <button onclick=giamSoLuong(${item.sanpham.id})><i class="fa-solid fa-angle-down"></i></button>
                                </td>
                                <td> ${item.sanpham.price * item.soluong} </td>
                                <td>
                                    <button onclick="deleteProduct(${item.sanpham.id})" > <i class="fa-solid fa-xmark"></i> </button>
                                </td>
                            </tr>
                        `;
    })
    document.getElementById("tblGioHang").innerHTML = trContent;
}

// FUNCTION: TĂNG SỐ LƯỢNG SẢN PHẨM TRONG GIỎ HÀNG
let themSoLuong = (id) => {
    for (let i = 0; i < gioHang.length; i++) {
        if (+gioHang[i].sanpham.id === id) {
            gioHang[i].soluong += 1;
        }
    }
    renderCart(gioHang);
    tongTienThanhToan();
    saveCartInLocal();
}

// FUNCTION: GIẢM SỐ LƯỢNG SẢN PHẨM TRONG GIỎ HÀNG
let giamSoLuong = (id) => {
    gioHang.forEach((item,index) => {
        if (+item.sanpham.id === id) {
            if (item.soluong > 1) {
                item.soluong -= 1;
            }else{
                gioHang.splice(index, 1);
            }
         }
    });
    renderCart(gioHang);
    tongTienThanhToan();
    saveCartInLocal();
}


// FUNCTION: TỔNG TIỀN THANH TOÁN GIỎ HÀNG
let tongTienThanhToan = () => {
    let total = 0;
    let cost = 0;
    gioHang.forEach(item => {
        cost = item.sanpham.price * item.soluong;
        total += cost;
    });
    document.getElementById("pay-money").innerHTML = `${total}đ`;
}

// FUNCTION: THANH TOÁN 
let thanhToan = () => {
    gioHang = [];
    saveCartInLocal();
    renderCart(gioHang);
    tongTienThanhToan();
}

// FUNCTION: LƯU GIỎ HÀNG XUỐNG LOCAL STORAGE
let saveCartInLocal = () => {
    // localStorage có 2 tham số: 1. key , 2. dữ liệu
    // chỉ nhận dữ liệu chuỗi, k lưu được mảng => chuyển sang JSON
    // cú pháp JSON.stringify()
    const jsonData = JSON.stringify(gioHang);
    // console.log(jsonData);
    localStorage.setItem("carts", jsonData);
}

// FUNCTION: LOAD DỮ LIỆU TỪ LOCAL
let getDataCart = () => {
    // Kiểm tra key có tồn tại không
    let dataJson = localStorage.getItem("carts"); //carts là key
    if (dataJson) {
        // nếu dữ liệu k null thì parse ra data
        let data = JSON.parse(dataJson);
        // sau đó renew data lại để có các method trong Object
        data.forEach(item => {
            let newProduct = new Product();
            newProduct.sanpham = item.sanpham;
            newProduct.soluong = item.soluong;
            gioHang.push(newProduct);
        });
        renderCart(gioHang);
        console.log("Giỏ Hàng:", gioHang);
    }
}

// FUNCTION: DELETE SẢN PHẨM TRONG GIỎ HÀNG
let deleteProduct = (id) => {
    let foundIndex = findProductById(id);
    if (foundIndex === -1) {
        alert(" id không tồn tại");
        return;
    }
    gioHang.splice(foundIndex, 1);
    saveCartInLocal();
    renderCart(gioHang);
    tongTienThanhToan();
}

// FUNCTION: HÀM TRẢ VỀ VỊ TRÍ SẢN PHẨM TRONG MẢNG GIỎ HÀNG
let findProductById = (id) => {
    gioHang.forEach((item, index) => {
        if (item.sanpham.id === id) {
            return index;
        }
        return -1;
    });
    
}

// Gán lại các function
window.themSanPhamVaoGioHang = themSanPhamVaoGioHang;
window.deleteProduct = deleteProduct;
window.thanhToan = thanhToan;
window.themSoLuong = themSoLuong;
window.giamSoLuong = giamSoLuong;




//================== LOADING DEFAULT =======================
// Load dữ liệu mặc định từ DB khi vào trang web
hienThiDanhSach();
getDataCart();
tongTienThanhToan();

